pymilter (1.0.6-2) unstable; urgency=medium

  * Team upload.
  * Python 3.13: Replace deprecated makeSuite() (closes: #1092753).
  * Run autopkgtests with /usr/sbin added to PATH, for makemap.
  * bsddb changed nulls in access file policy (from upstream; fixes test
    failure).
  * Use dh-sequence-python3.

 -- Colin Watson <cjwatson@debian.org>  Sun, 12 Jan 2025 16:38:30 +0000

pymilter (1.0.6-1) unstable; urgency=medium

  * Add d/source/options extend-diff-ignore to fix dpkg-source failure due to
    local changes (python package metadata regeneration) (Closes: #1045836)
  * New upstream release
  * Bump standards-version to 4.7.0 without further change

 -- Scott Kitterman <scott@kitterman.com>  Sat, 01 Jun 2024 09:09:01 -0400

pymilter (1.0.5-1) unstable; urgency=medium

  * New upstream release
    - Dropped all patches, in new release
  * Update debian/copyright
  * Add python3-setuptools to build-depends
  * Add python3-bsddb3 to recommends and test depends
  * Add sendmail-bin to test depends

 -- Scott Kitterman <scott@kitterman.com>  Wed, 29 Dec 2021 11:10:18 -0500

pymilter (1.0.4-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Set upstream metadata fields: Repository.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Remove constraints unnecessary since stretch:
    + python3-milter: Drop versioned constraint on postfix and sendmail in
      Suggests.
  * Update watch file format version to 4.
  * Bump debhelper from old 9 to 13.
  * Set upstream metadata fields: Archive, Contact, Name.

  [ Scott Kitterman ]
  * Move myself to uploaders and DPMT to maintainer
  * Update autopkgtest to run for all supported python versions
  * Bump standards-version to 4.6.0 without further change
  * Add debian/patches/milter_config_py3.patch to fix Milter/config.py python3
    compatibility (Closes: #994787)
  * Add debian/patches/py3_10_comnpat.patch to add upstream fixes for Python
    3.10 compatibility in the C extension

 -- Scott Kitterman <scott@kitterman.com>  Mon, 29 Nov 2021 11:50:22 -0500

pymilter (1.0.4-2) unstable; urgency=medium

  * Drop py2 autopkgtests since python2 support is dropped

 -- Scott Kitterman <scott@kitterman.com>  Tue, 09 Jul 2019 00:49:33 -0400

pymilter (1.0.4-1) unstable; urgency=medium

  * New upstream release
    - Dropped patches, incorporated upstream
  * Update d/control and d/rules to drop python-milter
    - No remaining python2 rdepends
  * Bump standards-version to 4.4.0 without further change
  * Set python-milter-doc to Multi-Arch: foreign

 -- Scott Kitterman <scott@kitterman.com>  Mon, 08 Jul 2019 09:28:02 -0400

pymilter (1.0.3-3) unstable; urgency=medium

  * Avoid crashes in Milter.utils.parseaddr (Closes: #922733)
  * add myself to uploaders

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Tue, 19 Feb 2019 18:35:31 -0500

pymilter (1.0.3-2) unstable; urgency=medium

  * Add preprocessor defines for kfreebsd and hurd
    - debian/patches/hurd_kfreebsd.patch

 -- Scott Kitterman <scott@kitterman.com>  Mon, 24 Dec 2018 03:59:34 -0500

pymilter (1.0.3-1) unstable; urgency=medium

  * New upstream release
    - Drop patches, incorporated upstream
  * Add autopkgtest to run upstream test suite
  * Bump standards-version too 4.3.0 without further change

 -- Scott Kitterman <scott@kitterman.com>  Mon, 24 Dec 2018 01:49:58 -0500

pymilter (1.0.2-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/control: Remove ancient X-Python-Version field
  * Convert git repository from git-dpm to gbp layout
  * d/watch: Use https protocol

  [ Scott Kitterman ]
  * Enable hardening flags
  * Update Homepage: and d/watch to point to new upstream location on Github
  * Bump standards-version to 4.2.1 without further change
  * New upstream release
  * Add support for python3:
    - Add new python3-milter binary package
    - Update debian/rules
    - For python3, install upstream's forward ported sgmllib in a private
      namespace
    - Add debian/patches/0001-Python-3-compatibility-fixes.patch to fix
      residual Python3 compatibility issues

 -- Scott Kitterman <scott@kitterman.com>  Fri, 21 Dec 2018 15:57:52 -0500

pymilter (1.0-2) unstable; urgency=medium

  * Rebuild against current libmilter to get socket activation support
    (Closes: #799736)
  * Update debian/rules to support separate arch/indep builds
  * Move doxygen to Build-Depends-Indep
  * Add dh-python to Build-Depends
  * Drop obsolete python-milter-docs Breaks/Replaces from python-milter-doc
  * Bump standards version to 3.9.6 without further change

 -- Scott Kitterman <scott@kitterman.com>  Wed, 23 Sep 2015 22:51:12 -0400

pymilter (1.0-1) unstable; urgency=medium

  [ Jakub Wilk ]
  * Fix a typo in README.source.
  * Fix a typo in the package description.

  [ Scott Kitterman ]
  * New upstream release
  * Bump standards version to 3.9.5 without further change

 -- Scott Kitterman <scott@kitterman.com>  Sat, 08 Mar 2014 22:21:24 -0500

pymilter (0.9.8-2) unstable; urgency=low

  * Upload to unstable
  * Add doc-base registration file
  * Drop no longer needed python-milter-docs transitional package
  * Update Vcs-* fields to point at their canonical locations

 -- Scott Kitterman <scott@kitterman.com>  Mon, 29 Apr 2013 17:21:01 -0400

pymilter (0.9.8-1) experimental; urgency=low

  * New upstream release
  * Drop unused python:Provides
  * Bump standards version to 3.9.4 without further change

 -- Scott Kitterman <scott@kitterman.com>  Fri, 22 Mar 2013 09:29:57 -0400

pymilter (0.9.7-1) experimental; urgency=low

  * New upstream release
    - Drop debian/patches/BSD-have-ipv6.diff, incorporated upstream
    - Remove debian/patches (no patches)
    - Drop quilt from build-depends and debian/rules
  * Bump debian/compat and debhelper version to 9

 -- Scott Kitterman <scott@kitterman.com>  Fri, 16 Nov 2012 01:44:53 -0500

pymilter (0.9.5-3) unstable; urgency=low

  * Add quilt as patch system (d/rules, d/control (build-dep), and
    README.source
  * Add debian/patches/BSD-have-ipv6.diff to fix 'initialization makes pointer
    from integer without a cast' warning on kFreeBSD

 -- Scott Kitterman <scott@kitterman.com>  Thu, 12 Apr 2012 11:52:34 -0400

pymilter (0.9.5-2) unstable; urgency=low

  * Update debian/copyright
  * Rename python-milter-docs to the more usual python-milter-doc and retain
    python-milter-docs as a transitional package until after wheezy
  * Update X-Python-Version to require python2.6 or later
  * Bump standards version to 3.9.3 without further change

 -- Scott Kitterman <scott@kitterman.com>  Sun, 18 Mar 2012 18:55:38 -0400

pymilter (0.9.5-1) unstable; urgency=low

  * New upstream release
  * Bump standards version to 3.9.2 without further change

 -- Scott Kitterman <scott@kitterman.com>  Sun, 11 Sep 2011 17:02:10 -0400

pymilter (0.9.4-1) unstable; urgency=low

  * New upstream release
    - Add doxygen to build-depends to build new documentation
    - Override dh_auto_build to get the docs build to work
    - Switch to building python-milter-docs from pymilter using the new
      doxygen based doc system
    - Add python-milter-docs to debian/control
    - Add python-milter-docs.install
    - Upate dh_auto_clean override to clean up after docs build
    - Override dh_auto_install to install python-milter files
  * Improve the package descriptions
  * Switch from XS-Python-Version to X-Python-Version
  * Drop XB-Python-Version
  * Bump standards version to 3.9.1 without further change

 -- Scott Kitterman <scott@kitterman.com>  Sat, 26 Mar 2011 02:50:02 -0400

pymilter (0.9.3-2) unstable; urgency=low

  * Convert from CDBS to Debhelper 7
    - Port debian/rules to DH 7 tiny plus overrides
    - Drop cdbs from build-depends
    - Bump debhelper version requirement
    - Change compat to 7
    - Remove cdbs generated pycompat
  * Convert from python-central to dh_python2
    - Drop build-depends on python-central
    - Version build-depend on python (>- 2.6.5.-2~)
    - Add --with python2 to debian/rules
  * Change python-milter-docs from Recommends to Suggests for python-milter
  * Specify versions in XS/B-Python-Version instead of all
  * Change source section to python and priority to optional
  * Improve short description
  * Remove unneeded README.source
  * Remove unused debian/pyversions
  * Bump standards version to 3.8.4 without further change
  * Drop DM-Upload-Allowed

 -- Scott Kitterman <scott@kitterman.com>  Thu, 24 Jun 2010 13:30:56 -0400

pymilter (0.9.3-1) unstable; urgency=low

  * New upstream release
  * Update standards version to 3.8.3 without further change

 -- Scott Kitterman <scott@kitterman.com>  Fri, 28 Aug 2009 11:45:16 -0400

pymilter (0.9.2-2) unstable; urgency=low

  * Simplify debian/rules by removing un-needed or redundant rules

 -- Scott Kitterman <scott@kitterman.com>  Tue, 28 Jul 2009 14:09:07 -0400

pymilter (0.9.2-1) unstable; urgency=low

  * New upstream release
    - Support additional milter functions
  * Update debian/copyright
  * Update standards version to 3.8.2 without further change

 -- Scott Kitterman <scott@kitterman.com>  Mon, 29 Jun 2009 09:40:50 -0400

pymilter (0.9.1-1) unstable; urgency=low

  * New upstream release

 -- Scott Kitterman <scott@kitterman.com>  Sun, 15 Feb 2009 16:53:06 -0500

pymilter (0.9.0-1) unstable; urgency=low

  [ Sandro Tosi ]
  * debian/control
    - switch Vcs-Browser field to viewsvn

  [ Scott Kitterman ]
  * New upstream release
    - Follow upstream split and drop provided milters (now in a separate
      milter tarball in the pymilter project, moved to pymilter-milters)
    - Add python-milter-docs to suggests (files formerly part of
      python-milter)
  * Add ${misc:Depends} to python-milter

 -- Scott Kitterman <scott@kitterman.com>  Sat, 27 Dec 2008 17:40:06 -0500

pymilter (0.8.10-1) unstable; urgency=low

  * New upstream release
    - Add new file temperror.txt to python-milter.examples

 -- Scott Kitterman <scott@kitterman.com>  Fri, 07 Nov 2008 23:38:35 -0500

pymilter (0.8.9-3) unstable; urgency=low

  * Bump standards version to 3.8.0.1
    - Add README.source
  * Swtich to python-central
    - Drop debian/pyversions and add XS/XB-Python-Version in debian/
      control
    - Update debian/rules and build-dep in debian/control
  * Move postfix/sendmail to suggests for python-milter and spf-milter-python
    - MTA need not run on the same system as the milter
  * Change priority to extra due to dependencies
  * Simplify init process to make it more robust

 -- Scott Kitterman <scott@kitterman.com>  Wed, 23 Jul 2008 16:28:21 -0400

pymilter (0.8.9-2) unstable; urgency=low

  [ Piotr Ożarowski ]
  * Rename XS-Vcs-* fields to Vcs-* (dpkg supports them now)
  * Added "XS-DM-Upload-Allowed: yes"

  [ Sandro Tosi ]
  * debian/control
    - uniforming Vcs-Browser field

  [ Scott Kitterman ]
  * Add binary package for the included spf-milter-python milter
    - Add debian/spf-milter-python* files
    - Add spf-milter-python binary to debian/control
    - Add debian/spfsetup.py to set up spf-milter-python
    - Adjust debian rules to build multiple binaries
    - Add debian/patches/spfmilter-debian-config so spf-milter-python
      works with package init script and has shebang
    - Add spfmilter.1 and debian/spf-milter-python.manpages
  * Bump standards version to 3.7.3 without further change
  * Remove unneeded debian/docs file
  * Change section of python-milter to python

 -- Scott Kitterman <scott@kitterman.com>  Thu, 01 May 2008 23:31:12 -0400

pymilter (0.8.9-1) unstable; urgency=low

  * Initial Debian package

 -- Scott Kitterman <scott@kitterman.com>  Tue, 25 Sep 2007 13:29:34 -0400
